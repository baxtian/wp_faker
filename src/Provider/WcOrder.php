<?php

# -*- coding: utf-8 -*-
/*
 * This file is part of the BrainFaker package.
 *
 * (c) Giuseppe Mazzapica
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Brain\Faker\Provider;

use Brain\Faker\MonkeyWcOrder;
use Brain\Monkey;
use DateTime;

class WcOrder extends FunctionMockerProvider
{
	/**
	 * @var array[]
	 */
	private $orders = [];

	/**
	 * @var bool
	 */
	private $currentOrderSet = false;

	private const ORDERS_STATUSES = [
		'wc-pending',
		'wc-processing',
		'wc-on-hold',
		'wc-completed',
		'wc-cancelled',
	];

	private const PAYMENT_METHODS = [
		'bacs',
	];

	/**
	 * @param \WC_Order $order
	 * @return callable
	 */
	public static function withSame(\WC_Order $order): callable
	{
		return function (\WC_Order $theOrder) use ($order): bool {
			return (int) $theOrder->ID === (int) $order->ID;
		};
	}

	/**
	 * @return void
	 */
	public function reset(): void
	{
		$this->orders          = [];
		$this->currentOrderSet = false;
		parent::reset();
	}

	/**
	 * @param array $properties
	 * @return \WC_Order|MonkeyWcOrder
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 */
	public function __invoke(array $properties = []): \WC_Order
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong

		$properties = array_change_key_case($properties, CASE_LOWER);

		$order      = $this->createBaseOrder($properties);
		$company    = $this->generator->company;
		$state      = $this->generator->state;
		$email      = $this->uniqueGenerator->email;
		$first_name = $this->generator->firstName;
		$last_name  = $this->generator->lastName;
		$billing    = (mt_rand(0, 1)) ? false : true;

		$defaults = [
			'status'              => $this->generator->randomElement(self::ORDERS_STATUSES),
			'payment_method'      => $this->generator->randomElement(self::PAYMENT_METHODS),
			'customer_id'         => $this->uniqueGenerator->numberBetween(1, 99999999),
			'user'                => $this->uniqueGenerator->numberBetween(1, 99999999),
			'user_id'             => $this->uniqueGenerator->numberBetween(1, 99999999),
			'total'               => $this->generator->randomNumber(2),
			'billing_address_1'   => $this->generator->streetAddress,
			'billing_address_2'   => $this->generator->secondaryAddress,
			'billing_city'        => $this->generator->city,
			'billing_company'     => $company,
			'billing_country'     => $this->generator->country,
			'billing_email'       => $email,
			'billing_first_name'  => $first_name,
			'billing_last_name'   => $last_name,
			'billing_phone'       => $this->generator->phoneNumber,
			'billing_postcode'    => $this->generator->postcode,
			'billing_state'       => $state,
			'shipping_address_1'  => (!$billing) ? '' : $this->generator->streetAddress,
			'shipping_address_2'  => (!$billing) ? '' : $this->generator->secondaryAddress,
			'shipping_city'       => (!$billing) ? '' : $this->generator->city,
			'shipping_company'    => (!$billing) ? '' : $this->generator->company,
			'shipping_country'    => (!$billing) ? '' : $this->generator->country,
			'shipping_first_name' => (!$billing) ? '' : $this->generator->firstName,
			'shipping_last_name'  => (!$billing) ? '' : $this->generator->lastName,
			'shipping_phone'      => (!$billing) ? '' : $this->generator->phoneNumber,
			'shipping_postcode'   => (!$billing) ? '' : $this->generator->postcode,
			'shipping_state'      => (!$billing) ? '' : $this->generator->state,
			'date_created'        => $this->generator->date('Y-m-d H:i:s'),
			'date_modified'       => $this->generator->date('Y-m-d H:i:s'),
			'date_completed'      => $this->generator->date('Y-m-d H:i:s'),
			'customer_note'       => $this->generator->sentence(),
		];

		foreach ($defaults as $key => $value) {
			$hasKey = array_key_exists($key, $properties);

			$field = $hasKey ? $properties[$key] : $value;
			if (strpos($key, 'date_') !== false) {
				$field = $this->generator->wp->datetime(['date' => $field]);
			}
			$order->{$key} = $field;
			$get[$key]     = $field;

			$order->shouldReceive('get_' . $key)
			->andReturnUsing(
				function () use ($key, $get) { //phpcs:ignore
					return array_key_exists($key, $get) ? $get[$key] : false;
				}
			)
			->byDefault();

			$order->shouldReceive('set_' . $key)
			->andReturnUsing(
				function ($value) use ($get) { //phpcs:ignore
					if (array_key_exists($key, $get)) {
						$get[$key] = $value;
					}
				}
			)
			->byDefault();
		}

		$arrays = [
			'coupons' => [
				'singular' => 'coupon',
				'num'      => 0,
			],
			'items' => [
				'singular' => 'item',
				'num'      => 0,
			],
		];

		foreach ($arrays as $key => $var) {
			$hasKey = array_key_exists($key, $properties);

			$num   = $hasKey ? $properties[$key] : $var['num'];
			$field = [];
			for ($i = 1; $i <= $num; $i++) {
				if ($key == 'coupons') {
					$field[] = $this->generator->wp->coupon();
				}
				if ($key == 'items') {
					$field[] = $this->generator->wp->orderItem();
				}
			}
			$order->{$key} = $field;
			$get[$key]     = $field;

			$order->shouldReceive('add_' . $var['singular'])
				->andReturnUsing(
					function ($item) use ($key, $get) { //phpcs:ignore
						if (array_key_exists($key, $get)) {
							$get[$key][] = $item;
						}
					}
				)
				->byDefault();

			$order->shouldReceive('get_' . $key)
				->andReturnUsing(
					function () use ($key, $get) { //phpcs:ignore
						return array_key_exists($key, $get) ? $get[$key] : false;
					}
				)
				->byDefault();
		}

		$order->shouldReceive('get_payment_method_title')
				->andReturnUsing(
					function () use ($get) { //phpcs:ignore
						if (array_key_exists('payment_method', $get)) {
							return $get['payment_method'];
						}
					}
				)
				->byDefault();

		$this->saveOrder($get, $order);
		$this->mockFunctions();

		return $order;
	}

	/**
	 * @param array $properties
	 * @return \Mockery\MockInterface|\WC_Order
	 */
	private function createBaseOrder(array $properties): \Mockery\MockInterface
	{
		$id = array_key_exists('id', $properties)
			? $properties['id']
			: $this->uniqueGenerator->numberBetween(1, 99999999);

		$order     = \Mockery::mock(\WC_Order::class);
		$order->ID = (int) $id;

		$order->shouldReceive('get_id')
			->andReturnUsing(
				function () use ($id) { //phpcs:ignore
					return $id;
				}
			)
			->byDefault();

		return $order;
	}

	/**
	 * @param array $properties
	 * @param int $siteId
	 * @param \WC_Order $order
	 */
	private function saveOrder(array $properties, \WC_Order $order)
	{
		$this->orders[$order->ID] = $properties;
	}

	/**
	 * @return void
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 * phpcs:disable Generic.Metrics.NestingLevel
	 */
	private function mockFunctions(): void
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong
		// phpcs:enable Generic.Metrics.NestingLevel

		if (!$this->canMockFunctions()) {
			return;
		}
	}
}
