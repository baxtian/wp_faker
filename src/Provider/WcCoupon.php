<?php

# -*- coding: utf-8 -*-
/*
 * This file is part of the BrainFaker package.
 *
 * (c) Giuseppe Mazzapica
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Brain\Faker\Provider;

use Brain\Faker\MonkeyWcCoupon;
use Brain\Monkey;

class WcCoupon extends FunctionMockerProvider
{
	/**
	 * @var array[]
	 */
	private $coupons = [];

	/**
	 * @var bool
	 */
	private $currentCouponSet = false;

	/**
	 * @param \WC_Coupon $coupon
	 * @return callable
	 */
	public static function withSame(\WC_Coupon $coupon): callable
	{
		return function (\WC_Coupon $theCoupon) use ($coupon): bool {
			return (int) $theCoupon->ID === (int) $coupon->ID;
		};
	}

	/**
	 * @return void
	 */
	public function reset(): void
	{
		$this->coupons          = [];
		$this->currentCouponSet = false;
		parent::reset();
	}

	/**
	 * @param array $properties
	 * @return \WC_Coupon|MonkeyWcCoupon
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 */
	public function __invoke(array $properties = []): \WC_Coupon
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong

		$properties = array_change_key_case($properties, CASE_LOWER);

		$coupon = $this->createBaseCoupon($properties);

		$meta = [
			'description' => $this->generator->text(),
			'amount'      => $this->generator->randomElement(['10', '20', '30']),
		];

		foreach ($meta as $key => $value) {
			$hasKey = array_key_exists($key, $properties);
			$field  = $hasKey ? $properties[$key] : $value;

			$coupon->{$key} = $field;
			$get[$key]      = $field;
		}

		$coupon->shouldReceive('get_meta')
			->with('coupon_data')
			->andReturnUsing(
				function ($format) use ($get, $meta) { //phpcs:ignore
					$answer = [];

					foreach ($meta as $key => $item) {
						$answer[$key] = (isset($get[$key])) ? $get[$key] : null;
					}

					return $answer;
				}
			)
			->byDefault();

		$this->saveCoupon($get, $coupon);
		$this->mockFunctions();

		return $coupon;
	}

	/**
	 * @param array $properties
	 * @return \Mockery\MockInterface|\WC_Coupon
	 */
	private function createBaseCoupon(array $properties): \Mockery\MockInterface
	{
		$id = array_key_exists('id', $properties)
			? $properties['id']
			: $this->uniqueGenerator->numberBetween(1, 99999999);

		$coupon     = \Mockery::mock(\WC_Coupon::class);
		$coupon->ID = (int) $id;

		$coupon->shouldReceive('get_id')
			->andReturnUsing(
				function () use ($id) { //phpcs:ignore
					return $id;
				}
			)
			->byDefault();

		return $coupon;
	}

	/**
	 * @param array $properties
	 * @param int $siteId
	 * @param \WC_Coupon $coupon
	 */
	private function saveCoupon(array $properties, \WC_Coupon $coupon)
	{
		$this->coupons[$coupon->ID] = $properties;
	}

	/**
	 * @return void
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 * phpcs:disable Generic.Metrics.NestingLevel
	 */
	private function mockFunctions(): void
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong
		// phpcs:enable Generic.Metrics.NestingLevel

		if (!$this->canMockFunctions()) {
			return;
		}
	}
}
