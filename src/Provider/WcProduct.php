<?php

# -*- coding: utf-8 -*-
/*
 * This file is part of the BrainFaker package.
 *
 * (c) Giuseppe Mazzapica
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Brain\Faker\Provider;

use Brain\Faker\MonkeyWcProduct;
use Brain\Monkey;

class WcProduct extends FunctionMockerProvider
{
	/**
	 * @var array[]
	 */
	private $products = [];

	/**
	 * @var bool
	 */
	private $currentProductSet = false;

	/**
	 * @param \WC_Product $product
	 * @return callable
	 */
	public static function withSame(\WC_Product $product): callable
	{
		return function (\WC_Product $theProduct) use ($product): bool {
			return (int) $theProduct->ID === (int) $product->ID;
		};
	}

	/**
	 * @return void
	 */
	public function reset(): void
	{
		$this->products          = [];
		$this->currentProductSet = false;
		parent::reset();
	}

	/**
	 * @param array $properties
	 * @return \WC_Product|MonkeyWcProduct
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 */
	public function __invoke(array $properties = []): \WC_Product
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong

		$properties = array_change_key_case($properties, CASE_LOWER);

		$product = $this->createBaseProduct($properties);

		$defaults = [
			'name' => $this->generator->text(),
			'sku'  => $this->generator->text(),
		];

		foreach ($defaults as $key => $value) {
			$hasKey = array_key_exists($key, $properties);
			$field  = $hasKey ? $properties[$key] : $value;

			$product->{$key} = $field;
			$get[$key]       = $field;

			$product->shouldReceive('get_' . $key)
			->andReturnUsing(
				function () use ($key, $get) { //phpcs:ignore
					return array_key_exists($key, $get) ? $get[$key] : false;
				}
			)
			->byDefault();

			$product->shouldReceive('set_' . $key)
			->andReturnUsing(
				function ($value) use ($get) { //phpcs:ignore
					if (array_key_exists($key, $get)) {
						$get[$key] = $value;
					}
				}
			)
			->byDefault();
		}

		$meta = [
			'poduct' => $this->generator->text(),
		];

		foreach ($defaults as $key => $value) {
			$hasKey = array_key_exists($key, $properties);
			$field  = $hasKey ? $properties[$key] : $value;

			if ($key == 'product') {
				$field = $this->generator->wp->product(['name' => $field]);
			}

			$product->{$key} = $field;
			$get[$key]       = $field;
		}

		$this->saveProduct($get, $product);
		$this->mockFunctions();

		return $product;
	}

	/**
	 * @param array $properties
	 * @return \Mockery\MockInterface|\WC_Product
	 */
	private function createBaseProduct(array $properties): \Mockery\MockInterface
	{
		$id = array_key_exists('id', $properties)
			? $properties['id']
			: $this->uniqueGenerator->numberBetween(1, 99999999);

		$product     = \Mockery::mock(\WC_Product::class);
		$product->ID = (int) $id;

		$product->shouldReceive('get_id')
			->andReturnUsing(
				function () use ($id) { //phpcs:ignore
					return $id;
				}
			)
			->byDefault();

		return $product;
	}

	/**
	 * @param array $properties
	 * @param int $siteId
	 * @param \WC_Product $product
	 */
	private function saveProduct(array $properties, \WC_Product $product)
	{
		$this->products[$product->ID] = $properties;
	}

	/**
	 * @return void
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 * phpcs:disable Generic.Metrics.NestingLevel
	 */
	private function mockFunctions(): void
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong
		// phpcs:enable Generic.Metrics.NestingLevel

		if (!$this->canMockFunctions()) {
			return;
		}
	}
}
