<?php

# -*- coding: utf-8 -*-
/*
 * This file is part of the BrainFaker package.
 *
 * (c) Giuseppe Mazzapica
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Brain\Faker\Provider;

use Brain\Faker\MonkeyWcOrderItem;
use Brain\Monkey;

class WcOrderItem extends FunctionMockerProvider
{
	/**
	 * @var array[]
	 */
	private $order_items = [];

	/**
	 * @var bool
	 */
	private $currentOrderItemSet = false;

	/**
	 * @param \WC_OrderItem $order_item
	 * @return callable
	 */
	public static function withSame(\WC_OrderItem $order_item): callable
	{
		return function (\WC_OrderItem $theOrderItem) use ($order_item): bool {
			return (int) $theOrderItem->ID === (int) $order_item->ID;
		};
	}

	/**
	 * @return void
	 */
	public function reset(): void
	{
		$this->order_items         = [];
		$this->currentOrderItemSet = false;
		parent::reset();
	}

	/**
	 * @param array $properties
	 * @return \WC_OrderItem|MonkeyWcOrderItem
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 */
	public function __invoke(array $properties = []): \WC_OrderItem
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong

		$properties = array_change_key_case($properties, CASE_LOWER);

		$order_item = $this->createBaseOrderItem($properties);

		$defaults = [
			'quantity' => $this->generator->text(),
		];

		foreach ($defaults as $key => $value) {
			$hasKey = array_key_exists($key, $properties);
			$field  = $hasKey ? $properties[$key] : $value;

			$order_item->{$key} = $field;
			$get[$key]          = $field;

			$order_item->shouldReceive('get_' . $key)
				->andReturnUsing(
					function () use ($key, $get) { //phpcs:ignore
						return array_key_exists($key, $get) ? $get[$key] : false;
					}
				)
				->byDefault();

			$order_item->shouldReceive('set_' . $key)
				->andReturnUsing(
					function ($value) use ($get) { //phpcs:ignore
						if (array_key_exists($key, $get)) {
							$get[$key] = $value;
						}
					}
				)
				->byDefault();
		}

		$meta = [
			'product' => $this->generator->text(),
		];

		foreach ($meta as $key => $value) {
			$hasKey = array_key_exists($key, $properties);
			$field  = $hasKey ? $properties[$key] : $value;

			if ($key == 'product') {
				$field = $this->generator->wp->product(['name' => $field]);
			}

			$order_item->{$key} = $field;
			$get[$key]          = $field;

			$order_item->shouldReceive('get_' . $key)
				->andReturnUsing(
					function () use ($key, $get) { //phpcs:ignore
						return array_key_exists($key, $get) ? $get[$key] : false;
					}
				)
				->byDefault();
		}

		$order_item->shouldReceive('get_meta')
			->with('_bundled_by')
			->andReturnUsing(
				function () use ($key, $get) { //phpcs:ignore
					return '';
				}
			)
			->byDefault();

		$this->saveOrderItem($get, $order_item);
		$this->mockFunctions();

		return $order_item;
	}

	/**
	 * @param array $properties
	 * @return \Mockery\MockInterface|\WC_OrderItem
	 */
	private function createBaseOrderItem(array $properties): \Mockery\MockInterface
	{
		$id = array_key_exists('id', $properties)
			? $properties['id']
			: $this->uniqueGenerator->numberBetween(1, 99999999);

		$order_item     = \Mockery::mock(\WC_OrderItem::class);
		$order_item->ID = (int) $id;

		$order_item->shouldReceive('get_id')
			->andReturnUsing(
				function () use ($id) { //phpcs:ignore
					return $id;
				}
			)
			->byDefault();

		return $order_item;
	}

	/**
	 * @param array $properties
	 * @param int $siteId
	 * @param \WC_OrderItem $order_item
	 */
	private function saveOrderItem(array $properties, \WC_OrderItem $order_item)
	{
		$this->order_items[$order_item->ID] = $properties;
	}

	/**
	 * @return void
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 * phpcs:disable Generic.Metrics.NestingLevel
	 */
	private function mockFunctions(): void
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong
		// phpcs:enable Generic.Metrics.NestingLevel

		if (!$this->canMockFunctions()) {
			return;
		}
	}
}
