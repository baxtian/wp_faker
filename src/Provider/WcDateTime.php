<?php

# -*- coding: utf-8 -*-
/*
 * This file is part of the BrainFaker package.
 *
 * (c) Giuseppe Mazzapica
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Brain\Faker\Provider;

use Brain\Faker\MonkeyWcDateTime;
use Brain\Monkey;
use DateTime;
use DateTimeZone;

class WcDateTime extends FunctionMockerProvider
{
	/**
	 * @var array[]
	 */
	private $datetimes = [];

	/**
	 * @var bool
	 */
	private $currentDateTimeSet = false;

	/**
	 * @param \WC_DateTime $datetime
	 * @return callable
	 */
	public static function withSame(\WC_DateTime $datetime): callable
	{
		return function (\WC_DateTime $theDateTime) use ($datetime): bool {
			return (int) $theDateTime->ID === (int) $datetime->ID;
		};
	}

	/**
	 * @return void
	 */
	public function reset(): void
	{
		$this->datetimes          = [];
		$this->currentDateTimeSet = false;
		parent::reset();
	}

	/**
	 * @param array $properties
	 * @return \WC_DateTime|MonkeyWcDateTime
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 */
	public function __invoke(array $properties = []): \WC_DateTime
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong

		$properties = array_change_key_case($properties, CASE_LOWER);

		$datetime = $this->createBaseDateTime($properties);

		$defaults = [
			'date'     => $this->generator->date('Y-m-d H:i:s'),
			'timezone' => 'UTC',
		];

		foreach ($defaults as $key => $value) {
			$hasKey = array_key_exists($key, $properties);
			$field  = $hasKey ? $properties[$key] : $value;

			$datetime->{$key} = $field;
			$get[$key]        = $field;
		}

		$datetime->shouldReceive('date')
			->andReturnUsing(
				function ($format) use ($get, $datetime) { //phpcs:ignore
					$date = new DateTime($get['date'], new DateTimeZone($get['timezone']));

					return $date->format($format);
				}
			)
			->byDefault();

		$datetime->shouldReceive('setTimezone')
			->andReturnUsing(
				function ($timezone) use ($get, $datetime) { //phpcs:ignore
					$get['timezone'] = $timezone;
				}
			)
			->byDefault();

		$this->saveDateTime($get, $datetime);
		$this->mockFunctions();

		return $datetime;
	}

	/**
	 * @param array $properties
	 * @return \Mockery\MockInterface|\WC_DateTime
	 */
	private function createBaseDateTime(array $properties): \Mockery\MockInterface
	{
		$id = array_key_exists('id', $properties)
			? $properties['id']
			: $this->uniqueGenerator->numberBetween(1, 99999999);

		$datetime     = \Mockery::mock(\WC_DateTime::class);
		$datetime->ID = (int) $id;

		$datetime->shouldReceive('get_id')
			->andReturnUsing(
				function () use ($id) { //phpcs:ignore
					return $id;
				}
			)
			->byDefault();

		return $datetime;
	}

	/**
	 * @param array $properties
	 * @param int $siteId
	 * @param \WC_DateTime $datetime
	 */
	private function saveDateTime(array $properties, \WC_DateTime $datetime)
	{
		$this->datetimes[$datetime->ID] = $properties;
	}

	/**
	 * @return void
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 * phpcs:disable Generic.Metrics.NestingLevel
	 */
	private function mockFunctions(): void
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong
		// phpcs:enable Generic.Metrics.NestingLevel

		if (!$this->canMockFunctions()) {
			return;
		}
	}
}
