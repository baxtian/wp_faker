<?php

# -*- coding: utf-8 -*-
/*
 * This file is part of the BrainFaker package.
 *
 * (c) Giuseppe Mazzapica
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Brain\Faker\Provider;

use Brain\Faker\MonkeyWcCustomer;
use Brain\Monkey;
use DateTime;

class WcCustomer extends FunctionMockerProvider
{
	/**
	 * @var array[]
	 */
	private $customers = [];

	/**
	 * @var bool
	 */
	private $currentCustomerSet = false;

	/**
	 * @param \WC_Customer $customer
	 * @return callable
	 */
	public static function withSame(\WC_Customer $customer): callable
	{
		return function (\WC_Customer $theCustomer) use ($customer): bool {
			return (int) $theCustomer->ID === (int) $customer->ID;
		};
	}

	/**
	 * @return void
	 */
	public function reset(): void
	{
		$this->customers          = [];
		$this->currentCustomerSet = false;
		parent::reset();
	}

	/**
	 * @param array $properties
	 * @return \WC_Customer|MonkeyWcCustomer
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 */
	public function __invoke(array $properties = []): \WC_Customer
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong

		$properties = array_change_key_case($properties, CASE_LOWER);

		$customer   = $this->createBaseCustomer($properties);
		$company    = $this->generator->company;
		$state      = $this->generator->state;
		$email      = $this->uniqueGenerator->email;
		$first_name = $this->generator->firstName;
		$last_name  = $this->generator->lastName;
		$billing    = (mt_rand(0, 1)) ? false : true;

		$defaults = [
			'email'               => $email,
			'date_created'        => $this->generator->date('Y-m-d H:i:s'),
			'date_modified'       => $this->generator->date('Y-m-d H:i:s'),
			'default_company'     => $company,
			'default_state'       => $state,
			'first_name'          => $first_name,
			'last_name'           => $last_name,
			'state'               => $state,
			'billing_address_1'   => $this->generator->streetAddress,
			'billing_address_2'   => $this->generator->secondaryAddress,
			'billing_city'        => $this->generator->city,
			'billing_company'     => $company,
			'billing_country'     => $this->generator->country,
			'billing_email'       => $email,
			'billing_first_name'  => $first_name,
			'billing_last_name'   => $last_name,
			'billing_phone'       => $this->generator->phoneNumber,
			'billing_postcode'    => $this->generator->postcode,
			'billing_state'       => $state,
			'shipping_address_1'  => (!$billing) ? '' : $this->generator->streetAddress,
			'shipping_address_2'  => (!$billing) ? '' : $this->generator->secondaryAddress,
			'shipping_city'       => (!$billing) ? '' : $this->generator->city,
			'shipping_company'    => (!$billing) ? '' : $this->generator->company,
			'shipping_country'    => (!$billing) ? '' : $this->generator->country,
			'shipping_first_name' => (!$billing) ? '' : $this->generator->firstName,
			'shipping_last_name'  => (!$billing) ? '' : $this->generator->lastName,
			'shipping_phone'      => (!$billing) ? '' : $this->generator->phoneNumber,
			'shipping_postcode'   => (!$billing) ? '' : $this->generator->postcode,
			'shipping_state'      => (!$billing) ? '' : $this->generator->state,
		];

		foreach ($defaults as $key => $value) {
			$hasKey = array_key_exists($key, $properties);

			$field = $hasKey ? $properties[$key] : $value;
			if (strpos($key, 'date_') !== false) {
				$field = $this->generator->wp->datetime(['date' => $field]);
			}
			$customer->{$key} = $field;
			$get[$key]        = $field;

			$customer->shouldReceive('get_' . $key)
			->andReturnUsing(
				function () use ($key, $get) { //phpcs:ignore
					return array_key_exists($key, $get) ? $get[$key] : false;
				}
			)
			->byDefault();

			$customer->shouldReceive('set_' . $key)
			->andReturnUsing(
				function ($value) use ($key, $get) { //phpcs:ignore
					if (array_key_exists($key, $get)) {
						$get[$key] = $value;
					}
				}
			)
			->byDefault();
		}

		$customer->shouldReceive('get_username')
			->andReturnUsing(
				function () use ($get) { //phpcs:ignore
					$username = sprintf('%s_%s', $get['first_name'], $get['last_name']);
					$username = strtolower(str_replace(' ', '_', $username));

					return $username;
				}
			)
			->byDefault();

		$this->saveCustomer($get, $customer);
		$this->mockFunctions();

		return $customer;
	}

	/**
	 * @param array $properties
	 * @return \Mockery\MockInterface|\WC_Customer
	 */
	private function createBaseCustomer(array $properties): \Mockery\MockInterface
	{
		$id = array_key_exists('id', $properties)
			? $properties['id']
			: $this->uniqueGenerator->numberBetween(1, 99999999);

		$customer     = \Mockery::mock(\WC_Customer::class);
		$customer->ID = (int) $id;

		$customer->shouldReceive('get_id')
			->andReturnUsing(
				function () use ($id) { //phpcs:ignore
					return $id;
				}
			)
			->byDefault();

		return $customer;
	}

	/**
	 * @param array $properties
	 * @param int $siteId
	 * @param \WC_Customer $customer
	 */
	private function saveCustomer(array $properties, \WC_Customer $customer)
	{
		$this->customers[$customer->ID] = $properties;
	}

	/**
	 * @return void
	 *
	 * phpcs:disable Inpsyde.CodeQuality.FunctionLength.TooLong
	 * phpcs:disable Generic.Metrics.NestingLevel
	 */
	private function mockFunctions(): void
	{
		// phpcs:enable Inpsyde.CodeQuality.FunctionLength.TooLong
		// phpcs:enable Generic.Metrics.NestingLevel

		if (!$this->canMockFunctions()) {
			return;
		}
	}
}
